package snoopaudio

import (
	"bytes"
	"encoding/binary"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"
	"time"

	"github.com/gorilla/websocket"

	"github.com/djherbis/stream"
)

const (
	// Time allowed to write the file to the client.
	writeWait = 10 * time.Second

	// pixel generator
	pixelGenPeriod = 1 * time.Second

	// Time allowed to read the next pong message from the client.
	pongWait = 60 * time.Second

	// Send pings to client with this period. Must be less than pongWait.
	pingPeriod = (pongWait * 9) / 10

	// Poll file for changes with this period.
	audioRatePeriod = 20 * time.Millisecond
	streamPeriod    = 20 * time.Millisecond

	// Wave header
	waveHeader = 44
	playHZ     = 44.1

	samplesPer20MS   = (128 * 8)
	bytesPer20MS     = samplesPer20MS * 2
	audioPlayPer20MS = samplesPer20MS / playHZ
)

type PixelData struct {
	Valid  bool  `json:"is_valid"`
	MaxVal int16 `json:"sample_max"`
	MinVal int16 `json:"sample_min"`
}

type AudioSampleData struct {
	ResetSamples  bool      `json:"reset_samples"`
	CurrentTime   float64   `json:"current_time"`
	TotalTime     float64   `json:"total_time"`
	DrawPixelData PixelData `json:"pixel_data"`
	AudioSamples  []int16   `json:"audio_samples"`
}

type TimelineSeek struct {
	SeekTo int64 `json:"seek_to"`
}

type LiveDataReceiver struct {
	cacheStream *stream.Stream // internal cache stream for seeking
	rawBytes    []byte         // bytes used to read from external source to cache stream

	seekRequestPosition int16

	byteAudioSamples []byte  // read the bytes from cache stream as bytes
	audioSamples     []int16 // convert the bytes to stream samples

	// Public variables
	socketConn *websocket.Conn // connected web socket
	rawReader  io.Reader       // generic reader from which we receive the actual input; mostly UDP

	// Sync
	closeEvent chan bool

	// json to send over sockets
	jsonAudioSampleData AudioSampleData

	// JUST FOR DEBUG
	RawSeeker io.Seeker
}

// CreateSnooper ..
func (liveDataReceiver *LiveDataReceiver) CreateSnooper(s *websocket.Conn, r io.Reader) error {

	if s == nil || r == nil {
		return errors.New("INvalid parameters")
	}

	liveDataReceiver.socketConn = s
	liveDataReceiver.rawReader = r

	var err error
	cTime := time.Now()
	streamName := fmt.Sprintf("livestream-%v-%v-%v-%v-%v-%v-%v", cTime.Year(), cTime.Month(), cTime.Day(), cTime.Hour(), cTime.Minute(), cTime.Second(), cTime.Nanosecond())
	// liveDataReceiver.cacheStream, err = stream.New(streamName)
	// Create an in-memory stream
	liveDataReceiver.cacheStream, err = stream.NewStream(streamName, stream.NewMemFS())
	if err != nil {
		fmt.Println(err)
		return err
	}

	liveDataReceiver.byteAudioSamples = make([]byte, bytesPer20MS)
	liveDataReceiver.rawBytes = make([]byte, bytesPer20MS)
	liveDataReceiver.audioSamples = make([]int16, samplesPer20MS)

	liveDataReceiver.seekRequestPosition = -1

	liveDataReceiver.jsonAudioSampleData.DrawPixelData.MaxVal = 0
	liveDataReceiver.jsonAudioSampleData.DrawPixelData.MinVal = 0
	liveDataReceiver.jsonAudioSampleData.DrawPixelData.Valid = false

	liveDataReceiver.closeEvent = make(chan bool)

	fmt.Println("Snooper created")

	// do some initializations for WebSockets
	// Ping pong handling settings
	liveDataReceiver.socketConn.SetReadLimit(512)
	liveDataReceiver.socketConn.SetReadDeadline(time.Now().Add(pongWait))

	// Set ping pong handler
	liveDataReceiver.socketConn.SetPongHandler(func(string) error {
		liveDataReceiver.socketConn.SetReadDeadline(time.Now().Add(pongWait))
		return nil
	})

	// Set close handler
	liveDataReceiver.socketConn.SetCloseHandler(func(code int, errMsg string) error {
		fmt.Printf("Close event fired with %v : message %v", code, errMsg)
		liveDataReceiver.cacheStream.Close()
		liveDataReceiver.closeEvent <- true
		return nil
	})

	return nil
}

// RunReceiverThread ..
func (liveDataReceiver *LiveDataReceiver) RunReceiverThread() {
	streamTicker := time.NewTicker(streamPeriod)

	defer func() {
		streamTicker.Stop()
		fmt.Println("Receiver thread closed")
	}()

	// invoke the raw data listener, which writes the stream
	for {
		select {
		case <-streamTicker.C:
			read, errRd := liveDataReceiver.rawReader.Read(liveDataReceiver.rawBytes)

			if errRd != nil {
				log.Println("Failed to read data from reader", errRd)
				// better we close if the input is done
				// NEED TO FINALYZE
			}

			if read != len(liveDataReceiver.rawBytes) {
				liveDataReceiver.RawSeeker.Seek(int64(waveHeader), 0)
				liveDataReceiver.rawReader.Read(liveDataReceiver.rawBytes[read:])
			}

			// write to stream
			_, err := liveDataReceiver.cacheStream.Write(liveDataReceiver.rawBytes)
			if err != nil {
				fmt.Println("Cannnot write to cache stream")
				continue
			}

		// Exit the thread on close event
		case <-liveDataReceiver.closeEvent:
			return
		}
	}
}

// RunSocketWriter spawns a thread which JSON with audio samples
func (liveDataReceiver *LiveDataReceiver) RunSocketWriter() error {
	pingTicker := time.NewTicker(pingPeriod)
	audioWriteTicker := time.NewTicker(audioRatePeriod)

	// Get the reader for the cache stream
	streamReader, errR := liveDataReceiver.cacheStream.NextReader()
	if errR != nil {
		log.Fatal(errR)
	}

	// invoke the pixel value generator
	go liveDataReceiver.runPixelValueGenerator()

	defer func() {
		pingTicker.Stop()
		audioWriteTicker.Stop()
		streamReader.Close()
		log.Println("WS writer thread closed")
	}()

	var seekNeeded int64
	for {
		select {
		case <-audioWriteTicker.C:

			// If the user seek set
			if liveDataReceiver.seekRequestPosition != -1 {
				if liveDataReceiver.seekRequestPosition == 100 {
					// Just move the pointer from the end of the cache with length of buffer
					streamReader.Seek(int64(len(liveDataReceiver.byteAudioSamples)), io.SeekEnd)
				} else {
					// Total size of the cache in bytes
					totalSize, _ := streamReader.Size()
					fTemp := float64(liveDataReceiver.seekRequestPosition) / 100
					fTemp = float64(float64(totalSize) * fTemp)
					seekNeeded = int64(fTemp)
					temp := int64(seekNeeded) % bytesPer20MS
					seekNeeded = seekNeeded - (bytesPer20MS - temp)
					if seekNeeded > 0 {
						// If there is not enough data to read for next play, seek back enough for its
						if (totalSize - seekNeeded) < bytesPer20MS {
							streamReader.Seek(bytesPer20MS, io.SeekEnd)
						} else {
							streamReader.Seek(seekNeeded, io.SeekStart)
						}
					}

					fmt.Printf("Seek requested %v achived %v", totalSize, seekNeeded)
				}
				liveDataReceiver.seekRequestPosition = -1
				liveDataReceiver.jsonAudioSampleData.ResetSamples = true
			} else {
				liveDataReceiver.jsonAudioSampleData.ResetSamples = false
			}

			// read the audio samples from the cache
			_, errA := streamReader.Read(liveDataReceiver.byteAudioSamples)
			if errA != nil {
				log.Println("End of stream reached, wait for next iteration")
				return nil
			}

			// Calaulate the total time and current time
			// get the seek pointer position for the time calculation
			playedBytes, _ := streamReader.Seek(0, io.SeekCurrent)
			inMilliSec := float64(playedBytes) / bytesPer20MS
			inMilliSec = inMilliSec * audioPlayPer20MS

			liveDataReceiver.jsonAudioSampleData.CurrentTime = inMilliSec

			playedBytes, _ = streamReader.Size()
			inMilliSec = float64(playedBytes) / bytesPer20MS
			inMilliSec = inMilliSec * audioPlayPer20MS

			liveDataReceiver.jsonAudioSampleData.TotalTime = inMilliSec

			// Convert to short samples
			binary.Read(bytes.NewReader(liveDataReceiver.byteAudioSamples), binary.LittleEndian, &liveDataReceiver.audioSamples)

			// Fill JSON
			liveDataReceiver.jsonAudioSampleData.AudioSamples = liveDataReceiver.audioSamples

			// Write it to socket
			liveDataReceiver.socketConn.SetWriteDeadline(time.Now().Add(writeWait))
			if err := liveDataReceiver.socketConn.WriteJSON(liveDataReceiver.jsonAudioSampleData); err != nil {
				liveDataReceiver.closeEvent <- true // trigger the close event
				log.Println("WS write failed, websocket is in invalid state", err)
				return nil
			}

			liveDataReceiver.jsonAudioSampleData.DrawPixelData.Valid = false

		// Ping-Pong thread
		case <-pingTicker.C:
			liveDataReceiver.socketConn.SetWriteDeadline(time.Now().Add(writeWait))
			if err := liveDataReceiver.socketConn.WriteMessage(websocket.PingMessage, []byte{}); err != nil {
				log.Println("Error writing ping message. websocket is in invalid state", err)
				liveDataReceiver.closeEvent <- true // trigger the close event
				return nil
			}

		// Exit the thread on close event
		case <-liveDataReceiver.closeEvent:
			return nil
		}
	}
}

// RunSocketReader : read from socket
func (liveDataReceiver *LiveDataReceiver) RunSocketReader() error {
	var timelineSeek TimelineSeek

	defer func() {
		fmt.Println("WS reader thread closed")
	}()

	for {
		select {

		// Exit the thread on close event
		case <-liveDataReceiver.closeEvent:
			return nil

		default:

			_, message, err := liveDataReceiver.socketConn.ReadMessage()
			if err != nil {
				log.Println("Read message from client failed, websocket is in invalid state", err)
				liveDataReceiver.closeEvent <- true // trigger the close event
				return nil
			}

			err = json.Unmarshal(message, &timelineSeek)
			if err != nil {
				log.Printf("Decode message from client failed %v %s \n", err, message)
				continue
			}

			liveDataReceiver.seekRequestPosition = int16(timelineSeek.SeekTo)
			fmt.Printf("Seeked to %v \n", liveDataReceiver.seekRequestPosition)
		}
	}
}

func (liveDataReceiver *LiveDataReceiver) runPixelValueGenerator() {
	pixelGenTicker := time.NewTicker(pixelGenPeriod)

	// Get the reader for the cache stream
	pixelStreamReader, errR := liveDataReceiver.cacheStream.NextReader()
	if errR != nil {
		log.Fatal(errR)
	}

	// data needed
	var slen int = 44100
	pixelRawBytes := make([]byte, slen*2)
	pixelAudioSamples := make([]int16, slen)
	var min int16 = 0
	var max int16 = 0

	// Clean-up
	defer func() {
		pixelGenTicker.Stop()
		pixelStreamReader.Close()
		fmt.Println("PIxel value calculator thread closed")
	}()

	for {
		select {
		case <-pixelGenTicker.C:
			// read the audio samples from the cache
			readC, errA := pixelStreamReader.Read(pixelRawBytes)
			if errA != nil || readC <= 0 {
				log.Println("End of stream reached, wait for next iteration")
				return
			}

			// Convert to short samples
			errA = binary.Read(bytes.NewReader(pixelRawBytes), binary.LittleEndian, &pixelAudioSamples)
			if errA != nil {
				log.Println("Error in convertion, wait for next iteration")
				continue
			}

			min = 0
			max = 0
			for i := 0; i < slen; i++ {
				// fmt.Printf("%v \t", audioSamples[i])
				if pixelAudioSamples[i] > max {
					max = pixelAudioSamples[i]
				} else if pixelAudioSamples[i] < min {
					min = pixelAudioSamples[i]
				}
			}

			//fmt.Printf("%v , %v \n", min, max)
			// assign the values
			liveDataReceiver.jsonAudioSampleData.DrawPixelData.Valid = true
			liveDataReceiver.jsonAudioSampleData.DrawPixelData.MinVal = (min)
			liveDataReceiver.jsonAudioSampleData.DrawPixelData.MaxVal = (max)

		// Exit the thread on close event
		case <-liveDataReceiver.closeEvent:
			return
		}
	}
}
