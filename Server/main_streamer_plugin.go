// Copyright 2013 The Gorilla WebSocket Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
	"encoding/binary"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"unsafe"

	"./snoopaudio"

	"github.com/gorilla/websocket"
)

// Global variable declaration
var (
	addr = flag.String("addr", ":8080", "http service address")

	filename string
	upgrader = websocket.Upgrader{
		ReadBufferSize:  1024,
		WriteBufferSize: 1024,
		CheckOrigin: func(r *http.Request) bool {
			return true
		},
	}

	audioFile AudioFile
)

type AudioFile struct {
	file     *os.File
	readTill uint32
}

type WaveHeader struct {
	RIFF          [4]byte
	ChunkSize     int32
	WAVE          [4]byte
	fmt           [4]byte
	Subchunk1Size int32
	AudioFormat   int16
	NumOfChan     int16
	SamplesPerSec int32
	bytesPerSec   int32
	blockAlign    int16
	bitsPerSample int16
	Subchunk2ID   [4]byte
	Subchunk2Size int32
}

func (audio *AudioFile) skipCurrentBytes(byteLen int64) error {

	// Go to the very begining
	_, err := audio.file.Seek(byteLen, 1)
	if err != nil {
		return err
	}

	return nil
}

func (audio *AudioFile) skipBytesFromBeginning(byteLen int64) error {

	// Go to the very begining
	_, err := audio.file.Seek(byteLen, 0)
	if err != nil {
		return err
	}

	return nil
}

// Read the file data
func (audio *AudioFile) readFile(data []byte) (read int, err error) {

	arrLen := len(data)
	read, err = audio.file.Read(data)

	if err != nil {
		return
	}

	if read == arrLen {
		return
	}

	//rem := arrLen - read
	audio.file.Seek(int64(unsafe.Sizeof(WaveHeader{})), 0)
	return audio.readFile(data[read:])
}

// Read the header of wave to struct
func (audio *AudioFile) readHeaderData(header *WaveHeader) (binaryHeader []byte, err error) {

	_, err = audio.file.Read(binaryHeader)
	if err != nil {
		log.Println("Cannot read header binary")
		return nil, err
	}

	binary.Read(audio.file, binary.BigEndian, &header.RIFF)
	binary.Read(audio.file, binary.LittleEndian, &header.ChunkSize)
	binary.Read(audio.file, binary.BigEndian, &header.WAVE)
	binary.Read(audio.file, binary.BigEndian, &header.fmt)
	binary.Read(audio.file, binary.LittleEndian, &header.Subchunk1Size)
	binary.Read(audio.file, binary.LittleEndian, &header.AudioFormat)
	binary.Read(audio.file, binary.LittleEndian, &header.NumOfChan)
	binary.Read(audio.file, binary.LittleEndian, &header.SamplesPerSec)
	binary.Read(audio.file, binary.LittleEndian, &header.bytesPerSec)
	binary.Read(audio.file, binary.LittleEndian, &header.blockAlign)
	binary.Read(audio.file, binary.LittleEndian, &header.bitsPerSample)
	binary.Read(audio.file, binary.BigEndian, &header.Subchunk2ID)
	binary.Read(audio.file, binary.LittleEndian, &header.Subchunk2Size)

	return
}

// Handles new connection request to WebSocket
func serveWs(w http.ResponseWriter, r *http.Request) {
	fmt.Println("New connection request")
	ws, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		if _, ok := err.(websocket.HandshakeError); !ok {
			log.Println(err)
		}
		return
	}

	// var header WaveHeader
	// go writer(ws, lastMod)
	// go reader(ws)
	dataReceiver := new(snoopaudio.LiveDataReceiver)
	if dataReceiver.CreateSnooper(ws, audioFile.file) == nil {

		audioFile.file.Seek(int64(44), 0) // DEBUG
		//audioFile.readHeaderData(&header)

		dataReceiver.RawSeeker = audioFile.file

		go dataReceiver.RunReceiverThread()

		go dataReceiver.RunSocketReader()

		go dataReceiver.RunSocketWriter()
	}
}

// main function
func main() {
	flag.Parse()
	if flag.NArg() != 1 {
		log.Fatal("filename not specified")
	}
	filename = flag.Args()[0]

	fmt.Println(filename)

	var err error
	audioFile.file, err = os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}

	fs := http.FileServer(http.Dir("./"))

	http.HandleFunc("/ws", serveWs)
	http.Handle("/", fs)

	fmt.Println("Started listening")
	if err = http.ListenAndServe(*addr, nil); err != nil {
		log.Fatal(err)
	}
}
