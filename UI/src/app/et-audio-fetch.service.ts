import { Injectable } from '@angular/core';
import { webSocket, WebSocketSubject } from 'rxjs/webSocket';


export interface PixelInfo {
  is_valid: boolean;
  sample_min: number;
  sample_max: number;
}

interface AudioData {
  reset_samples: boolean;
  current_time: number;
  total_time: number;
  pixel_data: PixelInfo;
  audio_samples: [];
}
@Injectable({
  providedIn: 'root'
})
export class ETAudioFetchService {

  private audioSamplesCB: (Object) => void;
  private pixelDataCB: (Object) => void;
  private timeInfoCB: (Object) => void;
  private audioConnection: WebSocketSubject<Object>;
  private seekPlay = {
    "seek_to": -1
  }

  constructor() {
    this.audioConnection = webSocket("ws://localhost:8080/ws");
  }

  connect(audioDataCB: (Object) => void, pDataCB?: (Object) => void, pTimeCB?: (Object) => void): void {
    this.audioSamplesCB = audioDataCB;
    this.pixelDataCB = pDataCB;
    this.timeInfoCB = pTimeCB;

    this.audioConnection.asObservable().subscribe(
      (audioData: AudioData) => {
        if (this.audioSamplesCB) {
          this.audioSamplesCB({
            dataSamples: audioData.audio_samples,
            clearBit: audioData.reset_samples
          });
        }

        if (this.timeInfoCB) {
          this.timeInfoCB({
            cur_time: audioData.current_time,
            tot_time: audioData.total_time
          })
        }

        if (audioData.pixel_data.is_valid && this.pixelDataCB) {
          this.pixelDataCB({
            min: audioData.pixel_data.sample_min,
            max: audioData.pixel_data.sample_max
          });
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }

  seekTo(percentage: number): void {
    if (percentage > 100 || percentage < 0)
      return;

    this.seekPlay.seek_to = percentage;

    // Send it
    this.audioConnection.next(this.seekPlay);
    // console.log(JSON.stringify(this.seekPlay));
  }
}
