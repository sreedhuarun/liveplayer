import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ETAudioPlayService {

  private playerController: ConstantSourceNode;
  private audioContext: AudioContext;
  private audioWorkletNode: AudioWorkletNode;

  constructor() { }


  async initAudio(): Promise<void> {

    this.audioContext = new AudioContext();

    if (this.audioContext.audioWorklet && typeof this.audioContext.audioWorklet.addModule === 'function') {

      try {
        await this.audioContext.audioWorklet.addModule('/assets/raw-audio-player.js');

        this.playerController = this.audioContext.createConstantSource();
        this.audioWorkletNode = new AudioWorkletNode(this.audioContext, 'raw-audio-player');

        if (this.playerController && this.audioWorkletNode) {
          this.playerController.connect(this.audioWorkletNode);
          this.audioWorkletNode.connect(this.audioContext.destination);

          this.playerController.start();
        } else {
          console.log("Cannot initialize player controls");
        }
      } catch (e) {
        console.log("Add module failed", e);
      }
    } else {
      console.log("Audio worklet not supported");
    }
  }

  addAudioData(data: Object): void {
    if (this.audioWorkletNode) {
      this.audioWorkletNode.port.postMessage(data);
    }
  }
}
