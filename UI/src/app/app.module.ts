import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ETLivePlayerComponent } from './et-live-player/et-live-player.component';
import { ETAudioFetchService } from './et-audio-fetch.service';
import { ETAudioPlayService } from './et-audio-play.service';

@NgModule({
  declarations: [
    AppComponent,
    ETLivePlayerComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [ETAudioFetchService, ETAudioPlayService],
  bootstrap: [AppComponent]
})
export class AppModule { }
