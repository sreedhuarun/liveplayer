import { TestBed } from '@angular/core/testing';

import { ETAudioFetchService } from './et-audio-fetch.service';

describe('ETAudioFetchService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ETAudioFetchService = TestBed.get(ETAudioFetchService);
    expect(service).toBeTruthy();
  });
});
