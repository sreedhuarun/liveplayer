import { TestBed } from '@angular/core/testing';

import { ETAudioPlayService } from './et-audio-play.service';

describe('ETAudioPlayService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ETAudioPlayService = TestBed.get(ETAudioPlayService);
    expect(service).toBeTruthy();
  });
});
