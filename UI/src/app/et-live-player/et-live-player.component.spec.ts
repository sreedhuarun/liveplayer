import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ETLivePlayerComponent } from './et-live-player.component';

describe('ETLivePlayerComponent', () => {
  let component: ETLivePlayerComponent;
  let fixture: ComponentFixture<ETLivePlayerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ETLivePlayerComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ETLivePlayerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
