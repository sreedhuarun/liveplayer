import { Component, OnInit, ViewChild, ElementRef, AfterViewInit, NgZone } from '@angular/core';
import { ETAudioPlayService } from '../et-audio-play.service';
import { ETAudioFetchService } from '../et-audio-fetch.service';

declare var PIXI: any; // instead of importing pixi like some tutorials say to do use declare

@Component({
  selector: 'et-live-player',
  templateUrl: './et-live-player.component.html',
  styleUrls: ['./et-live-player.component.css']
})

export class ETLivePlayerComponent implements OnInit, AfterViewInit {

  @ViewChild('waveFormCanvas', { static: true }) waveFormCanvasCont: ElementRef<HTMLDivElement>;

  public pApp: any; // this will be our pixi application

  private pixelArray = [];
  private SHORT_MAX = 32767;

  private pixelLoadedTill: number = 0;
  private audioWaveFromGraphics: any = null;
  private selectionGraphics: any = null;
  private transformMatrix: any = null;

  private currentTime: Date;
  private totalTime: Date;

  // Mouse dragging
  private isMouseDragActive: boolean = false;
  private activeSelectionStart: number = -1;
  private activeSelectionEnd: number = -1;
  private dragAnimationLoop: any = undefined;

  private audioPlayer: ETAudioPlayService = null;
  private audioDataService: ETAudioFetchService = null;

  constructor() {
    this.audioPlayer = new ETAudioPlayService();
    this.audioDataService = new ETAudioFetchService();
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.pApp = new PIXI.Application({
      width: this.waveFormCanvasCont.nativeElement.clientWidth,
      height: this.waveFormCanvasCont.nativeElement.clientHeight,
      backgroundColor: 0xeeeeee
    }); // this creates our pixi application

    // this places our pixi application onto the viewable document
    this.waveFormCanvasCont.nativeElement.appendChild(this.pApp.view);

    // Add event handlers to the canvas element
    let canvasElem: HTMLCanvasElement = (<HTMLCanvasElement>this.pApp.view);
    if (canvasElem != null && canvasElem != undefined) {
      canvasElem.addEventListener("click", this.onSeekPlay.bind(this));
      canvasElem.addEventListener("mousedown", this.onMouseDown.bind(this));
      canvasElem.addEventListener("mouseup", this.onMouseUp.bind(this));
      canvasElem.addEventListener("mousemove", this.onMouseDrag.bind(this));
    }

    // 2D Objects
    this.audioWaveFromGraphics = new PIXI.Graphics();
    this.selectionGraphics = new PIXI.Graphics();
    this.transformMatrix = new PIXI.Matrix(1, 0, 0, 1, 0, 0);

    this.pApp.stage.addChild(this.audioWaveFromGraphics);
    this.pApp.stage.addChild(this.selectionGraphics);
  }

  /**************** 
  * Event Handlers
  *********************/
  onSeekPlay(event: MouseEvent): void {
    if (event.offsetX > this.pixelLoadedTill)
      return;

    this.audioDataService.seekTo(Math.floor(
      (event.offsetX / this.pixelLoadedTill) * 100
    ));
  }

  onMouseDown(event: MouseEvent): void {
    if (event.button == 0 && this.isMouseDragActive == false) {
      this.isMouseDragActive = true;
      this.activeSelectionStart = event.offsetX;
    }
  }
  onMouseUp(event: MouseEvent): void {
    if (event.button == 0 && this.isMouseDragActive == true) {
      this.isMouseDragActive = false;
      this.activeSelectionStart = -1;
    }
  }
  onMouseDrag(event: MouseEvent): void {
    if (event.button == 0 && this.isMouseDragActive == true) {
      this.activeSelectionEnd = event.offsetX;
      setTimeout(this.drawWaveform.bind(this), 0);
    }
  }

  /**************************** 
   * private functions
  */

  startAudio(): void {
    this.initAudioPLayerAndService();

    this.audioDataService.connect(
      // Audio data for player
      (data) => {
        this.audioPlayer.addAudioData(data);
      },

      // Pixel info for UI
      (pixelInfo) => {
        this.pixelArray.push(pixelInfo.max / this.SHORT_MAX);
        this.pixelArray.push(pixelInfo.min / this.SHORT_MAX);
        this.pixelLoadedTill++;
      }
      ,

      (timeInfo) => {
        this.currentTime = new Date(0, 0, 0, 0, 0, 0, timeInfo.cur_time);
        this.totalTime = new Date(0, 0, 0, 0, 0, 0, timeInfo.tot_time);
      }
    );

    this.drawWaveform();
  }

  random(): number {
    return 100 + Math.random() * (this.SHORT_MAX - 100);
  }

  async initAudioPLayerAndService(): Promise<void> {
    await this.audioPlayer.initAudio();
  }

  drawWaveform(): void {

    // Remove the existing graphics object
    this.audioWaveFromGraphics.clear();
    this.selectionGraphics.clear();


    let midY = this.waveFormCanvasCont.nativeElement.clientHeight / 2;

    if (this.isMouseDragActive) {
      this.audioWaveFromGraphics.lineStyle(1, 0x888888, 0.5);
      this.selectionGraphics.lineStyle(1, 0xDE3249, 1);
    }
    else
      this.audioWaveFromGraphics.lineStyle(1, 0xDE3249, 1);

    // Load the pixel line
    let x = 1;
    for (let i = 1; i <= this.pixelArray.length; i += 2) {
      this.audioWaveFromGraphics.moveTo(x, Math.floor(midY + (midY * this.pixelArray[i - 1])));
      this.audioWaveFromGraphics.lineTo(x, Math.floor(midY + (midY * this.pixelArray[i])));
      ++x;
    }

    // Draw a disable rectangle
    if (this.pixelLoadedTill < this.waveFormCanvasCont.nativeElement.clientWidth) {
      this.audioWaveFromGraphics.lineStyle(1, 0x888888, 0.5);
      // The center line
      this.audioWaveFromGraphics.moveTo(this.pixelLoadedTill, midY);
      this.audioWaveFromGraphics.lineTo(this.waveFormCanvasCont.nativeElement.clientWidth, midY);
    }

    // If the total pixel exceeds the width of Canvas, we scale (sqeeze) the canvas
    if (this.pixelLoadedTill > this.waveFormCanvasCont.nativeElement.clientWidth) {
      //this.audioWaveFromGraphics.transform.setFromMatrix();
      this.transformMatrix.identity();
      this.transformMatrix.scale(this.waveFormCanvasCont.nativeElement.clientWidth / this.pixelLoadedTill, 1);
      this.audioWaveFromGraphics.transform.setFromMatrix(this.transformMatrix);
    }

    if (this.isMouseDragActive) {
      let start = Math.min(this.activeSelectionStart, this.activeSelectionEnd);
      let end = Math.max(this.activeSelectionStart, this.activeSelectionEnd);

      let x = start;
      for (let i = start * 2;
        i <= this.pixelArray.length &&
        i <= (end * 2);
        i += 2) {
        this.selectionGraphics.moveTo(x, Math.floor(midY + (midY * this.pixelArray[i - 1])));
        this.selectionGraphics.lineTo(x, Math.floor(midY + (midY * this.pixelArray[i])));
        ++x;
      }

      this.selectionGraphics.lineStyle(0, 0xDE3249);

      if (start > 0) {
        // this.selectionGraphics.moveTo(start, 0);
        // this.selectionGraphics.lineTo(start, this.waveFormCanvasCont.nativeElement.clientHeight);
        this.selectionGraphics.beginFill(0x888888, 0.1);
        this.selectionGraphics.drawRect(0, 0,
          start,
          this.waveFormCanvasCont.nativeElement.clientHeight);
        this.selectionGraphics.endFill();
      }

      if (end < this.waveFormCanvasCont.nativeElement.clientWidth) {
        // this.selectionGraphics.moveTo(end, 0);
        // this.selectionGraphics.lineTo(end, this.waveFormCanvasCont.nativeElement.clientHeight);

        this.selectionGraphics.beginFill(0x888888, 0.1);
        this.selectionGraphics.drawRect(end, 0,
          this.waveFormCanvasCont.nativeElement.clientWidth,
          this.waveFormCanvasCont.nativeElement.clientHeight);
        this.selectionGraphics.endFill();
      }
    }

    // if (this.isMouseDragActive) {
    //   let start = Math.min(this.mouseDragStart, this.mouseDragEnd);
    //   let end = Math.max(this.mouseDragStart, this.mouseDragEnd);

    //   let x = start;
    //   for (let i = start * 2;
    //     i <= this.pixelArray.length &&
    //     i <= (end * 2);
    //     i += 2) {
    //     this.selectionGraphics.moveTo(x, Math.floor(midY + (midY * this.pixelArray[i - 1])));
    //     this.selectionGraphics.lineTo(x, Math.floor(midY + (midY * this.pixelArray[i])));
    //     ++x;
    //   }
    // }

    // Add a blur filter
    if (this.isMouseDragActive)
      this.audioWaveFromGraphics.filters = [new PIXI.filters.BlurFilter()];
    else
      this.audioWaveFromGraphics.filters = null;



    // revoke it
    setTimeout(this.drawWaveform.bind(this), 1000);
  }

}
