
/**
 * This enques the individual array and process it as single array.
 *
 * @class FIFOBuffer
 * 
 */
export default class FIFOBuffer {

    constructor() {
        this._currentArray = null;
        this._currentOffset = -1;
        this._arrayQueue = [];
    }

    clear() {
        let len = this._arrayQueue.length;
        for (let i = 0; i < len; i++) {
            this._arrayQueue.shift();
        }
    }

    push(arraySeq) {
        this._arrayQueue.push(arraySeq);
    }

    pull(outArray) {

        let outArrayLen = (outArray.length)

        // Invalid out array
        if (outArray == null || outArray == undefined || outArrayLen <= 0) {
            console.log("Invalid params")
            return;
        }

        // Nothing pushed yet or we wait for 200 milliseconds
        if (this._arrayQueue.length <= 0) {
            console.log("Empty queue");
            return;
        }

        if (this._currentArray == null) {
            this._currentArray = this._arrayQueue.shift();
            this._currentOffset = 0;
        }

        // If the data in current array is not enough
        let remBytes = (this._currentArray.length - this._currentOffset);
        if (remBytes < outArrayLen) {
            // Read the remaining
            let outIndex = 0;
            for (; outIndex < remBytes;) {
                outArray[outIndex++] = this._currentArray[this._currentOffset++] / 32768;
            }

            // Take another buffer
            if (this._arrayQueue.length > 0) {
                this._currentArray = this._arrayQueue.shift();
                this._currentOffset = 0;
            }
            else {
                console.log("Queue exhausted");
                return;
            }

            // read remaining from new array
            remBytes = outArrayLen - remBytes;
            for (let i = 0; i < remBytes; i++) {
                outArray[outIndex++] = this._currentArray[this._currentOffset++] / 32768;
            }

        } else {
            let outIndex = 0;
            for (; outIndex < outArrayLen;) {
                outArray[outIndex++] = this._currentArray[this._currentOffset++] / 32768;
            }
        }
    }
}

export {
    FIFOBuffer,
};