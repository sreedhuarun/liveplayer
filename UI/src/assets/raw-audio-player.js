import FIFOBuffer from './FIFOBuffer.js';

/**
 * An audio player plays directly samples from WebSockets.
 *
 * @class RawAudioPlayer
 * @extends AudioWorkletProcessor
 */
class RawAudioPlayer extends AudioWorkletProcessor {

  constructor() {
    super();
    this._internalBuffer = new FIFOBuffer();

    this.port.onmessage = (event) => {
      if (event.data.clearBit) {
        this._internalBuffer.clear();
      }
      this._internalBuffer.push(new Int16Array(event.data.dataSamples));
    };
  }

  process(inputs, outputs, parameters) {
    let output = outputs[0];
    let input = inputs[0];

    // Play with s single channel
    let outputChannel = output[0];
    const inputChannel = input[0];

    //console.log(inputChannel.length, outputChannel.length);
    // for (let i = 0; i < outputChannel.length && inputChannel.length ; ++i) {
    //   // This loop can branch out based on AudioParam array length, but
    //   // here we took a simple approach for the demonstration purpose.
    //   outputChannel[i] = inputChannel[i] * 2 * (Math.random() - 0.5) ;
    // }

    this._internalBuffer.pull(outputChannel);

    //console.log("Out put")
    //console.log(outputChannel);

    return true;
  }
}

registerProcessor('raw-audio-player', RawAudioPlayer);
